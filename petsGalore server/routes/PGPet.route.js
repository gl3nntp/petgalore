'use strict'
const express = require('express'),
petRoute = express.Router();
//user schema and db collection to be used
let PGpet = require('../models/petModel');

/*
[ requesters: { _id: 5d3c46052e096b133c194988,
    requesterId: '5d211725f88286457c06a917',
    requesterUserName: 'glenn',
    requestStatus: 'pending',
    requestRejectMessage: '' } ],
__v: 0 }*/

petRoute.route('/deletePetProfile').delete( (req, res) => {
    console.log(`profile to delete: ${req.query.petId}`);
    PGpet.findByIdAndDelete(req.query.petId, (err, result) => {
        res.status(200).json({ message : 'the pet profile has been deleted', error : false })
    });
});

petRoute.route('/deleteAdoptionRequest').patch( (req, res) => { 
    console.log(`got from client: ${req.body.uId}`);
    PGpet.findById(req.body.pId,{} ,(err, result) => {
        //console.log(`before delete ${result.requesters}`);
        const indexToRemove = result.requesters.findIndex( request => request.requesterId === req.body.uId);
        result.requesters.splice(indexToRemove, 1);

        PGpet.updateOne({ _id : result._id}, result, (err, doc) => {

            console.log('updated');
            if (err) return res.status(200).json({ message : err, error : true });
            else return res.status(200).json({ message : 'your adoption request has been deleted', error : false });
        });
        //console.log(`after to delete ${result.requesters}`);
    });
});

petRoute.route('/getMyAdoptionRequests').get( (req, res) => {
 //console.log(`id got from client for requests: ${req.query.ownerId}`);
 PGpet.find({ requesters : {$elemMatch: {requesterId : req.query.ownerId}}},(err, result) => {
    //console.log(`my requested pets ${result}`);
    if(err) console.log(`there was an error retrieving data`);
    else res.status(200).json(result);
 });
});

petRoute.route('/getMyPets').get( (req, res) => {
    //console.log(`id got from client: ${req.query.ownerId}`);
    PGpet.find({ originalOwnerId : req.query.ownerId }, (err, result) => {
        if(err){
            console.log(err);
        }
        else{
            res.status(200).json(result);
        }
    }).catch( (err) => { `failed to get from db. ${err}`});
});

petRoute.route('/requestAdoption').patch( (req, res) => {
    PGpet.findById( req.body.petId, (err, result) =>{
        result.requesters.push(req.body.adoptRequest);
        console.log(result.requesters);
        PGpet.updateOne({ _id : result._id}, result, (err, doc) => {

            console.log('added');
            if (err) return res.status(200).json({ message : err, error : true });
            else return res.status(200).json({ message : 'your adoption request has been sent', error : false });
        });
    });
});

petRoute.route('/getPets').get( (req, res) => {
//when sending an http get request from client
//with params property in the http options (not with /:var )
//in express, USE req.query and not req.params

const longitude = parseFloat(req.query.longitude);
const latitude = parseFloat(req.query.latitude);
//const currentUser = req.query.user;

const petList = PGpet.aggregate([{
      $geoNear: {
         near: { type: "Point", coordinates: [longitude, latitude] },
         key: "location",
         distanceField: "dist.calculated",
         distanceMultiplier : 0.001
      }
    }]);

petList.exec( (err, result) => {

    result.forEach( (pet) => {

        pet.dist.calculated = Number((pet.dist.calculated).toFixed(2));

    });

    if (err) {
        console.log(`error occured in geosearch: ${err}`);
    } else {
        //console.log(`got from client: long ${longitude} lat ${latitude} user ${currentUser}`);
        //console.log(JSON.stringify(res, 0, 2));
        res.status(200).json(result);
    };
    
});

/*PGpet.find({
    location: {
     $near: {
      $geometry: {
       type: "Point",
       coordinates: [req.query.longitude, req.query.latitude]
      }
     }
    }
   }).find((error, results) => {
    if (error) console.log(`error occured in geosearch: ${error}`);
    console.log(JSON.stringify(results, 0, 2));
   });*/
});

petRoute.route('/addPet').post( (req, res) => {
    let NewPet = new PGpet(req.body);
    //console.log(`data received from client: ${NewPet}`);
    NewPet.save({},(err,result) => {
        if(err){
            console.log(`error adding to dbdfhg! : ${err}`);
            //res.status(400).json(`unable to save new user: ${err}`);
        }
        else{
            console.log(`pet successfuly added to db! : ${result}`);
            res.status(200).json(`pet successfuly added to db!`);
        }
    });
});

module.exports = petRoute;