'use strict'
const express = require('express'),
userRoute = express.Router();
const jwt = require('jsonwebtoken');
//user schema and db collection to be used
let PGuser = require('../models/userModel');

//authenticate user
userRoute.route('/user/verify').post( (req, res) => {
    console.log(req.body.userLog);
    PGuser.findOne({email: req.body.userLog}, (err, result) => {

        if (result != null) {
           console.log(`email found: ${result}`);
           if (req.body.password === result.password) {
                console.log(`successfuly logged in`);
                const token = jwt.sign(result.email, result.password);
                console.log(`token produced: ${token}`);
                res.status(200).json({error : false,
                                     result : token,
                                   userName : result.userName,
                                     userId : result._id});
           }
           else {
                console.log(`wrong password`);
                res.status(200).json({ error : true,
                                      result : `passwordError`});
           }
        }
        else {
            console.log(`email not found.`);
            res.status(200).json({ error : true,
                                  result : `emailError`});
        }

    }).catch(err => {
        res.status(400).json(`${err}: Unable to connect`);
    });
});

//add new user
userRoute.route('/add').post( function(req, res){
    let NewUser = new PGuser(req.body);
    NewUser.save({},(err,product) => {
        if(err){
            res.status(400).json(`unable to save new user: ${err}`);
        }
        else{
            res.status(200).json(true);
        }
    });
});

//? get route
userRoute.route('/test').get( function(req, res) {
    PGuser.find(function(err, user){
        if(err){
            console.log(err);
        }
        else{
            res.status(200).json(user);
        }
    });
});

// route for checking email availability
userRoute.route('/user/:email').get( function(req, res) {
    PGuser.findOne({email: req.params.email}, (err, result) =>{
        
        res.status(200).json(result);

    }).catch( err => {
        res.status(400).json(`${err}: Unable to connect`);
    });
});

// route for checking user name availability
userRoute.route('/userName/:userName').get( function(req, res) {
    PGuser.findOne({userName: req.params.userName}, (err, result) =>{
        
        // console.log(`got from db: ${result}`)
        res.status(200).json(result);

    }).catch( err => {
        res.status(400).json(`${err}: Unable to connect`);
    });
});

module.exports = userRoute;