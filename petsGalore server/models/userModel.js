const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let User = new Schema({
    firstName: { type: String },
    lastName: { type: String },
    userName: { type: String },
    password: { type: String },
    email: { type: String }
}, {collection: 'userList'});

module.exports = mongoose.model('User', User);