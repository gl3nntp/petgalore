const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let loc = new Schema({
    type: {
      type: String,
      enum: ['Point']
    },
    coordinates: {
      type: [Number, Number]
    }
});

let request = new Schema({
    requesterId: { type: String },
    requesterUserName: { type: String },
    requestStatus: {
      type: String,
      enum: ['pending', 'approved', 'rejected'] },
    requestRejectMessage: { type: String }
});

/*export interface IAdoptionRequest {
  _id ?: string;
  requesterId : string;
  requesterUserName : string;
  requestStatus : 'pending' | 'approved' | 'rejected';
  requestRejectMessage : string;
}*/

let petProfile = new Schema({
    originalOwnerId : { type : String },
    adoptionStatus : { type : String, enum : ['available','adopted'] },
    petName : { type : String },
    petKind : { type : String },
    petAdditionalDetails : { type : String },
    petProfilePic : { type : String },
    petAddress : { type : String },
    location : { type : loc },
    requesters : { type : [request] }
}, {collection: 'petList'});

petProfile.index({location : '2dsphere'});

module.exports = mongoose.model('petProfile', petProfile);