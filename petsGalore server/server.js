const express = require('express'),
bodyParser = require('body-parser'),
mongoose = require('mongoose'),
cors = require('cors'),
config = require('./DB'),
userRoute = require('./routes/PGUser.route');
petRoute = require('./routes/PGPet.route');

// Connect to MongoDB using mongoose:
mongoose.Promise = global.Promise;
mongoose.connect(config.DB, {useNewUrlParser: true, useCreateIndex : true}).then(
    () => {console.log('Database connected!');},
    err => {console.log('Failed to connect to database ' + err);}
);

const app = express();
app.use(bodyParser.json());
app.use(cors());
app.use('/PG', userRoute);
app.use('/PGpet', petRoute);
let port = process.env.PORT || 4000;
app.listen(port, function(){
    console.log('Listening on port: ' + port);
});