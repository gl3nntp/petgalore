import { Injectable } from '@angular/core';

import { AbstractControl, ValidatorFn, ValidationErrors} from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class SyncValidatorsService {

  constructor() { }

  ValidatePassword() : ValidatorFn{
    return (control: AbstractControl): ValidationErrors | null => {
      const firstPW = control.get('password').value;
      const secondPW = control.get('passwordVerify').value;
      if(firstPW === secondPW){
        //console.log(`ok first pw: ${firstPW} second pw:${secondPW}`);
        return null;
      }
      else{
        //console.log(`error first pw: ${firstPW} second pw:${secondPW}`);
        return {'passwordNotMatch' : true};
      }
    }
  }

  ValidateEmailFormat() : ValidatorFn{
    return (control: AbstractControl): ValidationErrors | null => {
      const emailInput = control.value;
      const regEx : RegExp = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
      if(regEx.test(emailInput)){
        return null;
      }
      else{
        return {'emailFormatError' : true};
      }
    }
  }

}
