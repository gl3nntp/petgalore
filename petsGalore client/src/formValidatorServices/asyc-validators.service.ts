import { Injectable } from '@angular/core';
import { UsersService } from '../httpServices/users.service';
import { AbstractControl, AsyncValidator, ValidationErrors } from '@angular/forms';
import { Observable } from 'rxjs';
import { map, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AsyncValidate implements AsyncValidator {

  constructor(private httpReq : UsersService) { }

  //check the db if email already exists
  validate(control: AbstractControl): Observable<ValidationErrors | null> {
    return this.httpReq.checkUserByEmail(control.value).pipe(
      map( httpVal => { 
          if(httpVal === null){
            return null;
          }
          else{
            return {'emailNotAvailable' : true};
          }
        }),
        catchError(() => null)
    );
  }

  //check the db if user name already exists
  validateUserName(control: AbstractControl): Observable<ValidationErrors | null> {
    return this.httpReq.checkUserByUser(control.value).pipe(
      map( httpVal => { 
          if(httpVal === null){
            return null;
          }
          else{
            return {'UserNameNotAvailable' : true};
          }
        }),
        catchError(() => null)
    );
  }

}
