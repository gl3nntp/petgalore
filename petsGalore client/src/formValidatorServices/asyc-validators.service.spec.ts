import { TestBed } from '@angular/core/testing';

import { AsyncValidate } from './asyc-validators.service';

describe('AsycValidatorsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AsyncValidate = TestBed.get(AsyncValidate);
    expect(service).toBeTruthy();
  });
});
