import { TestBed } from '@angular/core/testing';

import { SyncValidatorsService } from './sync-validators.service';

describe('SyncValidatorsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SyncValidatorsService = TestBed.get(SyncValidatorsService);
    expect(service).toBeTruthy();
  });
});
