import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { share } from 'rxjs/operators'
import { IUserAuthenticated } from '../model/user.model';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class UserStatusService {

//this is a simple state management using rxjs behaviourSubject

  //initial user state
  private initialUserState : IUserAuthenticated = {
      userName : '',
      userId : '',
      webToken : '',
      isAuthenticated : false
    }; 

  constructor( private route : Router) { }

  private userAuth = new BehaviorSubject<IUserAuthenticated>(this.initialUserState);

  //geters and setters for the userAuth:

  public get currentUser() : Observable<IUserAuthenticated> {

    if( !this.hasValidToken() ){
      this.userLogOut();
    }

    return this.userAuth.asObservable().pipe(share());
    
  }

  public get rawUser() {

    return this.userAuth.value

  }

  //methods:
  userLogIn(_userName, _webToken, _userId){

    /*var locStoreObj : IUserAuthenticated = JSON.parse(localStorage.getItem('authenticatedUser'));
    console.log(`before log in behaviorSubject value ${this.userAuth.value.userName}`);
    console.log(`before log in localStorage value ${locStoreObj}`);*/
    
    const currentUser : IUserAuthenticated = {
      userName : _userName,
      userId : _userId,
      webToken : _webToken,
      isAuthenticated : true
    };

    this.userAuth.next({...currentUser});

    localStorage.setItem('authenticatedUser', JSON.stringify({
      userName : this.userAuth.value.userName,
      userId : this.userAuth.value.userId,
      webToken : this.userAuth.value.webToken })
    );

    //this.route.navigate(['userProfile']);
    /*console.log(`after log in behaviorSubject value ${this.userAuth.value.userName}`);
    console.log(`after log in local storage value ${locStoreObj.userName}`);*/

  }

  userLogOut(){

    localStorage.removeItem('authenticatedUser');
    this.userAuth.next({...this.initialUserState});
    this.route.navigate(['home']);

  }

  private hasValidToken() : boolean {

    //console.log('reached user.status.service / hasValidToken()');
    //console.log(this.userAuth.value);
    const localStorageCredential :  IUserAuthenticated = JSON.parse(localStorage.getItem('authenticatedUser'));

    /*console.log(`behaviorSubject value in hasValidToken() ${this.userAuth.value.userName}`);
    console.log(`localStorage value in hasValidToken() ${localStorageCredential}`);*/


    if ( localStorageCredential !== null ){

        localStorageCredential.isAuthenticated = true;
        this.userAuth.next({...localStorageCredential});
        //console.log(`valid user: ${localStorageCredential.isAuthenticated}`)
        return true;

    } else { 
        
        return false;

    };
     
  }

}
