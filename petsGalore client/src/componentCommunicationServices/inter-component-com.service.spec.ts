import { TestBed } from '@angular/core/testing';

import { InterComponentComService } from './inter-component-com.service';

describe('InterComponentComService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: InterComponentComService = TestBed.get(InterComponentComService);
    expect(service).toBeTruthy();
  });
});
