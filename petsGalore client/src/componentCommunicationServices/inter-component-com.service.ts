import { Injectable } from '@angular/core';
import { Subject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class InterComponentComService {

  private _listner = new Subject<any>();

    eventListen(): Observable<any> {
       return this._listner.asObservable();
    }

    dataPassed(data : any) {
       this._listner.next(data);
    }

  constructor() { }
}
