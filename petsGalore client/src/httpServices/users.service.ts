import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { IUserRegister, IUser, IpetProfile, IAdoptionRequest } from '../model/user.model';
import { IuserAuthServerResult } from '../model/http.models';
import { share } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  uri = 'http://localhost:4000/PG';
  petUri = 'http://localhost:4000/PGpet';

  constructor(private http : HttpClient) { }

  deletePetProfile(pId : string){
    return this.http.delete<{message : string, error : boolean}>(`${this.petUri}/deletePetProfile`,
    { params: {petId : pId} });
  }

  deleteAdoptionRequest(pId : string, uId : string){
    //console.log(`id to be sent to server: ${_petId} ${_userId}`);
    return this.http.patch<{message : string, error : boolean}>(`${this.petUri}/deleteAdoptionRequest`,
    { pId, uId } );
  }

  getMyAdoptionRequests(userId : string){
    return this.http.get<IpetProfile[]>(`${this.petUri}/getMyAdoptionRequests`,
    { params: {ownerId : userId} });
  }

  getMyPets(userId : string){
    //console.log(`id to be sent to server: ${userId}`);
    return this.http.get<IpetProfile[]>(`${this.petUri}/getMyPets`,
    { params: {ownerId : userId} });
  }

  requestAdoption(petId : string, adoptRequest : IAdoptionRequest){
    //console.log(`request to be sent to db:`);
    //console.log(`pet ID: ${petId} request: ${adoptRequest.requestStatus}  ${adoptRequest.requesterId}  ${adoptRequest.requesterUserName}`);
    return this.http.patch<{message : string, error : boolean}>(`${this.petUri}/requestAdoption`, {petId, adoptRequest});
  }

  getPetsNearMe(long, lat){
    //console.log(`got from component ${long} ${lat}`)
    return this.http.get<IpetProfile[]>(`${this.petUri}/getPets`,
    { params: {longitude : long, latitude : lat}} );
  }

  addPet(pet : IpetProfile){
    return this.http.post(`${this.petUri}/addPet`, pet).pipe(share());
  }

  //adds a new user to the database
  addUser(user : IUserRegister) {
    const newUser : IUser = {
      firstName : user.firstName,
      lastName : user.lastName,
      userName : user.userName,
      password : user.password,
      email : user.email.toLowerCase()
    };
    return this.http.post(`${this.uri}/add`, newUser);
  }

  //user log-in validation - returns a JWT token
  userAuth(_userLog, _password){
    const userToCheck = {userLog: _userLog, password: _password};
    //console.log(`to be sent to db: ${userToCheck.userLog}`);
    return this.http.post<IuserAuthServerResult>(`${this.uri}/user/verify`, userToCheck);
  }

  getUsers() {
    return this.http.get<IUser[]>(`${this.uri}/test`);
  }

  //used for form validation if email is available:
  checkUserByEmail(email) {
    return this.http.get(`${this.uri}/user/${email}`);
  }

  //used for form validation if user name is available:
  checkUserByUser(userName) {
    return this.http.get(`${this.uri}/userName/${userName}`);
  }

}
