import { Component, OnInit, OnDestroy } from '@angular/core';
import { LocationService } from '../../locationServices/location.service';
import { ICoordinates } from '../../model/location.models';

import { BsModalService, PageChangedEvent } from 'ngx-bootstrap';
import { PetProfileModalComponent } from '../pet-profile-modal/pet-profile-modal.component';
import { UsersService } from '../../httpServices/users.service';
import { IpetProfile } from '../../model/user.model';
import { UserStatusService } from '../../states/user.status.service';
import { InterComponentComService } from '../../componentCommunicationServices/inter-component-com.service';

@Component({
  selector: 'app-adopt',
  templateUrl: './adopt.component.html',
  styleUrls: ['./adopt.component.css']
})
export class AdoptComponent implements OnInit, OnDestroy {

  userLoc : ICoordinates;
  petsNearMe : IpetProfile[] = [];
  //////////////////pagination
  returnedArray: IpetProfile[];
  
  constructor(private modalService: BsModalService,
                  private tracker : LocationService,
                     private http : UsersService,
                     private user : UserStatusService,
                 private interCom : InterComponentComService) {

    this.interCom.eventListen().subscribe( (dataPassed : any) => {
    console.log(`from adopt component ${dataPassed}`);
    console.log(`after ngOninit call`);
    //this.ngOnInit();
    window.location.reload();
    });

  }

  ngOnInit(){

    this.tracker.trackMe().subscribe( (coords : ICoordinates) => {
    this.userLoc = coords;
    this.fetchPets();
    });

  }

  fetchPets(){

    //this.user.currentUser.subscribe().unsubscribe();
    const identity : string = this.user.rawUser.userId;

    const { longitude, latitude } = this.userLoc;

    this.http.getPetsNearMe(longitude, latitude).subscribe( 
      ( pets : IpetProfile[] ) =>{

        pets.forEach( (pet) => {
          if(pet.originalOwnerId !== identity){
            if(pet.adoptionStatus === "available"){
              if(!pet.requesters.some( (request) => { return request.requesterId === identity } )){
                this.petsNearMe.push(pet);
          }}} 
        });
        //////////////////pagination
        this.returnedArray = this.petsNearMe.slice(0, 10);
    });

   }

  //////////////////pagination
  pageChanged(event: PageChangedEvent): void {
    const startItem = (event.page - 1) * event.itemsPerPage;
    const endItem = event.page * event.itemsPerPage;
    this.returnedArray = this.petsNearMe.slice(startItem, endItem);
  }

  openPetProfileModal(pet : IpetProfile) {

    const initialState = {
      currentUserId : this.user.rawUser.userId,
      currentUserName : this.user.rawUser.userName,
      petId : pet._id,
      image : pet.petProfilePic,
      name : pet.petName,
      kind : pet.petKind,
      distance : pet.dist.calculated,
      details : pet.petAdditionalDetails
    };

    console.log(initialState);

    this.modalService.show(
      PetProfileModalComponent,
      Object.assign({}, { class: 'gray modal-lg' , initialState })
    );
  }

  ngOnDestroy(): void {
    //throw new Error("Method not implemented.");
  }

}
