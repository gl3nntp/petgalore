import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { ShopComponent } from './shop/shop.component';
import { AdoptComponent } from './adopt/adopt.component';
import { HomeComponent } from './home/home.component';
import { AdoptWidgetComponent } from './home/adopt-widget/adopt-widget.component';
import { ShopWidgetComponent } from './home/shop-widget/shop-widget.component';
import { SignInComponent } from './sign-in-modal/sign-in.component';
import { RegisterComponent } from './register/register.component';

//animation modules
import { ParallaxScrollModule } from 'ng2-parallaxscroll';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

//ngx-bootstrap components
import { ModalModule,
         TabsModule,
         PopoverModule,
         AccordionModule,
         PaginationModule} from 'ngx-bootstrap';

import { HttpClientModule } from '@angular/common/http';

//google maps core module for geocoding
import { AgmCoreModule } from '@agm/core';
import { PetProfileModalComponent } from './pet-profile-modal/pet-profile-modal.component';
import { UserProfileComponent } from './user-profile/user-profile.component';
import { CreatePetProfileComponent } from './user-profile/create-pet-profile/create-pet-profile.component';

import { UserStatusService } from '../states/user.status.service';
import { PetsForAdoptionComponent } from './user-profile/pets-for-adoption/pets-for-adoption.component';
import { AdoptionRequestsComponent } from './user-profile/adoption-requests/adoption-requests.component';

@NgModule({
  declarations: [
    AppComponent,
    ShopComponent,
    AdoptComponent,
    HomeComponent,
    AdoptWidgetComponent,
    ShopWidgetComponent,
    SignInComponent,
    RegisterComponent,
    PetProfileModalComponent,
    UserProfileComponent,
    CreatePetProfileComponent,
    PetsForAdoptionComponent,
    AdoptionRequestsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ParallaxScrollModule,
    ModalModule.forRoot(),
    TabsModule.forRoot(),
    PopoverModule.forRoot(),
    AccordionModule.forRoot(),
    PaginationModule.forRoot(),
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    AgmCoreModule.forRoot({
      apiKey : 'AIzaSyB6GCM71g5GcfWqYsxoSA2GeoZ4m5KSnZM',
      libraries : ['places']
    })
  ],
  //modal component needs to be added here
  entryComponents : [
    PetProfileModalComponent,
    SignInComponent
  ],
  providers: [UserStatusService],
  bootstrap: [AppComponent]
})
export class AppModule { }
