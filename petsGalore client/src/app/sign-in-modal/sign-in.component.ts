import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { UsersService } from '../../httpServices/users.service';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { IuserAuthServerResult } from '../../model/http.models';
import { UserStatusService } from '../../states/user.status.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.css']
})
export class SignInComponent implements OnInit {

  //this component is a modal.

  userLogIn : FormGroup;
  modalRef : BsModalRef;
  message : string;

  //these varibales are properties of the initialStatus object of this modal component
  //initialStatus property to check where this component was loaded from.
  whoCalledSignInModal : string;
  //initialStatus property for value passed by component.
  userComponentValue : string;

  constructor( private logInUser : UsersService,
               private _modalRef : BsModalRef,
                    private user : UserStatusService,
                   private route : Router) {}

  ngOnInit() {

    this.modalRef = this._modalRef;

    //form initialization
    this.userLogIn = new FormGroup({
      userLog : new FormControl(''),
      password : new FormControl('')
    });

    //initializing the message and form values according to who loaded the modal
    switch (this.whoCalledSignInModal) {

      //if the modal was loaded in the register component,
      //the email of the user is set into the email field and the field is disabled
      //the user now only need to enter password
      case 'RegisterComponent' : {
        this.message = 'You have succesfuly registered! please sign in';
        this.userLogIn.get('userLog').setValue(this.userComponentValue);
        this.userLogIn.get('userLog').disable();
        break;
      };

      //if the modal was loaded in the app component,
      //the user needs to enter both email and password for authentication
      case 'AppComponent' || 'PetProfileModalComponent' : {
        this.message = 'Welcome to Pets Galore! please sign in';
        break;
      };

      default : { break };
        
    };

  }

  //user clicks log-in button
  logIn(){

    //values from the form fields are taken to be sent to the web api for authentication
    const email : string = this.userLogIn.get('userLog').value;
    const password : string = this.userLogIn.get('password').value;

    //the response is an object with error : boolean type and the result : string type
    //if there is an error with the authentication, the object response will have error property as true
    //and result property will be what kind of error : either wrong email or wrong password
    //if the authentication succeeds, the return is error as false and result property would be a JWT token
    this.logInUser.userAuth(email.toLowerCase(), password).subscribe(

      (response : IuserAuthServerResult) => {

        const { error, result, userName, userId } = response;
        
          if (error) {

            switch (result) {

              case 'emailError' : {
                this.userLogIn.get('userLog').setErrors( { emailError : true } );
                //console.log(`email status = unregistered email : ${this.userLogIn.get('userLog').status}`);
                break;
              };

              case 'passwordError' : {
                this.userLogIn.get('password').setErrors( { passwordError : true } );
                //console.log(`password status = incorrect password : ${this.userLogIn.get('password').status}`);
                break;
              };

              default : { break };

            };

          } else {

            /*console.log(`authentication success ${this.userLogIn.status}`);
            console.log(`token pruduced ${response.result} for user ${response.userName}`);*/

            this.user.userLogIn(userName, result, userId);
            
            if(this.whoCalledSignInModal === 'PetProfileModalComponent'){
              this.modalRef.hide();
            } 
            else {
              this.modalRef.hide();
              this.route.navigate(['userProfile']);
            }
            

          }
      });
  }

}
