import { Component, OnInit, ViewChild, ElementRef, NgZone, Output, EventEmitter, OnDestroy } from '@angular/core';
import { FormBuilder, Validators, FormGroup} from '@angular/forms';
import { addressList, IAddress } from './addresses';
import { profilePics } from './profilePics';
import { IpetProfile, IgeoJSON, IUserAuthenticated } from '../../../model/user.model';
import { UserStatusService } from '../../../states/user.status.service';
import { UsersService } from '../../../httpServices/users.service';
import { InterComponentComService } from '../../../componentCommunicationServices/inter-component-com.service';
//import { MapsAPILoader } from '@agm/core';

@Component({
  selector: 'app-create-pet-profile',
  templateUrl: './create-pet-profile.component.html',
  styleUrls: ['./create-pet-profile.component.css']
})
export class CreatePetProfileComponent implements OnInit, OnDestroy{

  private readonly petTypeList = ['dog','cat','fish','hamster','mice','bird','reptile'];
  private readonly adrList = addressList;
  private readonly pics = profilePics; 
  private newPetForAdoption : IpetProfile;
  petProfileForm : FormGroup;

  @Output() updateEvent : EventEmitter<any> = new EventEmitter();

  constructor(
          private fb : FormBuilder,
        private user : UserStatusService,
        private http : UsersService,
    private interCom : InterComponentComService
  ){}

  ngOnInit() {

    this.petProfileForm = this.fb.group({
      petName : ['', Validators.required],
      petType : ['', Validators.required],
      petAddress : ['', Validators.required],
      tellMorePet : ['', Validators.required],
      petPicUrl : ['', Validators.required]
    });

  }

  //form control getters:
  public get petName() { return this.petProfileForm.get('petName') };
  public get petType() { return this.petProfileForm.get('petType') };
  public get petAddress() { return this.petProfileForm.get('petAddress') };
  public get tellMorePet() { return this.petProfileForm.get('tellMorePet') };
  public get petPicUrl() { return this.petProfileForm.get('petPicUrl') };

  setProfilePic(picUrl : string){
    this.petPicUrl.setValue(picUrl);
    //console.log(`image url ${picUrl}`);
  }
  
  registerPet(){
    
    this.sendToWebAPI(this.user.rawUser);
    
  }

  sendToWebAPI(petOwner : IUserAuthenticated){
    
    const adrs : IAddress = this.petAddress.value;
    const geojson : IgeoJSON = {
      type : "Point",
      coordinates : [adrs.longitude, adrs.latitude]
    };

    this.newPetForAdoption = {
      originalOwnerId : petOwner.userId,
      adoptionStatus : 'available',
      petName : this.petName.value,
      petKind : this.petType.value,
      petAdditionalDetails : this.tellMorePet.value,
      petProfilePic : this.petPicUrl.value,
      petAddress : adrs.address,
      location : geojson
    };

    this.http.addPet(this.newPetForAdoption).subscribe( (result) => {
      console.log(result);
      alert(`${this.newPetForAdoption.petName} has been successfuly registered`);
      this.petProfileForm.reset();
      this.updateEvent.emit('update');
      this.interCom.dataPassed('update');
      //window.location.reload();
    });

  }

  ngOnDestroy(): void {
  }

}

  /*@ViewChild('petLoc')
  private searchElementRef: ElementRef;*/

  /*constructor(
    private mapsAPILoader: MapsAPILoader,
    private ngZone: NgZone
  ) { }*/

  /*mapAutocomplete(){
    this.mapsAPILoader.load().then(() => {

      const autocomplete = new google.maps.places.Autocomplete(this.searchElementRef.nativeElement, {
        types: ["petLoc"]
      });

      autocomplete.addListener("searchAddress", () => {
        this.ngZone.run(() => {
          let place: google.maps.places.PlaceResult = autocomplete.getPlace();

          //verify result
          if (place.geometry === undefined || place.geometry === null) {
            return;
          }

        });
      });
    });
  }*/

