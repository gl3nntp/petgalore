export interface IAddress {
    address : string;
    longitude : number;
    latitude : number;
}

export const addressList : IAddress[] = [
    { address : 'Address 1', longitude : 34.779504, latitude : 32.056456 },
    { address : 'Address 2', longitude : 34.774141, latitude : 32.079366 },
    { address : 'Address 3', longitude : 34.786610, latitude : 32.094718 },
    { address : 'Address 4', longitude : 34.8117922, latitude : 32.0710843 },
    { address : 'Address 5', longitude : 34.8232484, latitude : 32.0719777 },
    { address : 'Address 6', longitude : 34.8586252, latitude : 32.0578164 },
    { address : 'Address 7', longitude : 34.8540618, latitude : 32.0296449 },
    { address : 'Address 8', longitude : 34.7888979, latitude : 32.0115481 },
    { address : 'Address 9', longitude : 34.7678253, latitude : 31.9667437 },
    { address : 'Address 10', longitude : 34.8392365, latitude : 31.9425819 },
    { address : 'Address 11', longitude : 34.8412862, latitude : 32.1515463 },
    { address : 'Address 12', longitude : 34.9076564, latitude : 32.1849296 },
    { address : 'Address 13', longitude : 34.8802036, latitude : 32.2133318 },
    { address : 'Address 14', longitude : 34.8408973, latitude : 32.1723456 }
];