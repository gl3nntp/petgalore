import { Component, OnInit } from '@angular/core';
import { IAdoptionRequest, IpetProfile } from '../../../model/user.model';
import { UsersService } from '../../../httpServices/users.service';
import { UserStatusService } from '../../../states/user.status.service';

@Component({
  selector: 'app-adoption-requests',
  templateUrl: './adoption-requests.component.html',
  styleUrls: ['./adoption-requests.component.css']
})
export class AdoptionRequestsComponent implements OnInit {

  myRequestedPets : IpetProfile[] = [];
  
  constructor(private http : UsersService,
              private user : UserStatusService) { }

  ngOnInit() {

    this.user.currentUser.subscribe().unsubscribe();
    const currUser = this.user.rawUser.userId;

    this.http.getMyAdoptionRequests(this.user.rawUser.userId).subscribe( (requested : IpetProfile[]) => {
      requested.forEach( (profile : IpetProfile) => {
        const request : IAdoptionRequest = profile.requesters.find( (req : IAdoptionRequest) => { return req.requesterId === currUser} );
        profile.requesters = [];
        profile.requesters.push(request);
        //console.log(`user requester : ${profile.requesters.length}`);
      });

      this.myRequestedPets = requested;
      //console.log(this.myRequestedPets);

    });
  }

  deleteRequest(petId : string, userId : string){
    this.http.deleteAdoptionRequest(petId, userId).subscribe( ({message, error}) => {
      if(error) alert('there was a problem deleting your request');
      else alert(message);
      this.ngOnInit();
    });
    //console.log(`delete request from id ${petId} delete request of ${userId}`);
  }

}
