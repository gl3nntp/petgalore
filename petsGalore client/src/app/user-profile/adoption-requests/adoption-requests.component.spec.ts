import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdoptionRequestsComponent } from './adoption-requests.component';

describe('AdoptionRequestsComponent', () => {
  let component: AdoptionRequestsComponent;
  let fixture: ComponentFixture<AdoptionRequestsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdoptionRequestsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdoptionRequestsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
