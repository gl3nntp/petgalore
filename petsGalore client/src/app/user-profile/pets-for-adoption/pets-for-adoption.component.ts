import { Component, OnInit, OnDestroy } from '@angular/core';
import { IpetProfile, IUserAuthenticated } from '../../../model/user.model';
import { UsersService } from '../../../httpServices/users.service';
import { UserStatusService } from '../../../states/user.status.service';
import { InterComponentComService } from '../../../componentCommunicationServices/inter-component-com.service';

@Component({
  selector: 'app-pets-for-adoption',
  templateUrl: './pets-for-adoption.component.html',
  styleUrls: ['./pets-for-adoption.component.css']
})
export class PetsForAdoptionComponent implements OnInit, OnDestroy {

  myPets : IpetProfile[] = [];
 
  constructor( private http : UsersService,
               private user : UserStatusService,
           private interCom : InterComponentComService) { 
    this.interCom.eventListen().subscribe((dataPassed : any) => {
    //console.log(dataPassed);
    this.getPets(this.user.rawUser);
    });
  }

  ngOnInit() {
    this.user.currentUser.subscribe(
      (user) => { this.getPets(user); }
    )
  }

  getPets(user : IUserAuthenticated){
    //console.log(`current user to fetch pets: ${user.userId}`)
    this.http.getMyPets(user.userId).subscribe( (result : IpetProfile[]) => {
      this.myPets = result;
    });
  }

  deletePetProfile(petId){
    //console.log(`profile to delete ${petId}`);
    this.http.deletePetProfile(petId).subscribe( ({message, error}) => {
      alert(message);
      this.ngOnInit();
    });
  }

  ngOnDestroy(): void {
  }

}
