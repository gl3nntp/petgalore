import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PetsForAdoptionComponent } from './pets-for-adoption.component';

describe('PetsForAdoptionComponent', () => {
  let component: PetsForAdoptionComponent;
  let fixture: ComponentFixture<PetsForAdoptionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PetsForAdoptionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PetsForAdoptionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
