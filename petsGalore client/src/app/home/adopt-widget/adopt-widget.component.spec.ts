import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdoptWidgetComponent } from './adopt-widget.component';

describe('AdoptWidgetComponent', () => {
  let component: AdoptWidgetComponent;
  let fixture: ComponentFixture<AdoptWidgetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdoptWidgetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdoptWidgetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
