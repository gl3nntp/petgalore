import { Component, OnInit } from '@angular/core';
import { BsModalService } from 'ngx-bootstrap';
import { PetProfileModalComponent } from '../../pet-profile-modal/pet-profile-modal.component';
import { UsersService } from '../../../httpServices/users.service';
import { LocationService } from '../../../locationServices/location.service';
import { ICoordinates } from '../../../model/location.models';
import { IpetProfile, IUserAuthenticated } from '../../../model/user.model';
import { UserStatusService } from '../../../states/user.status.service';
import { InterComponentComService } from '../../../componentCommunicationServices/inter-component-com.service';


@Component({
  selector: 'app-adopt-widget',
  templateUrl: './adopt-widget.component.html',
  styleUrls: ['./adopt-widget.component.css']
})
export class AdoptWidgetComponent implements OnInit {

  userLoc : ICoordinates;
  petsNearMe : IpetProfile[];

  constructor(private modalService: BsModalService,
                  private tracker : LocationService,
                     private http : UsersService,
                     private user : UserStatusService,
                 private interCom : InterComponentComService) {
    this.interCom.eventListen().subscribe((dataPassed : any) => {
    console.log(dataPassed);
    //this.ngOnInit();
    
    });
  }

  ngOnInit(){

    this.tracker.trackMe().subscribe( (coords : ICoordinates) => {
      this.userLoc = coords;
      this.fetchPets();
    });

  }

  fetchPets(){

    //this.user.currentUser.subscribe().unsubscribe();
    const identity : string = this.user.rawUser.userId;
    console.log(`current identity: ${identity}`);

    const { longitude, latitude } = this.userLoc;

    this.http.getPetsNearMe(longitude, latitude).subscribe( 
      ( pets : IpetProfile[] ) =>{

        let copy : IpetProfile[] = [];

        pets.forEach( (pet) => {
          if(pet.originalOwnerId !== identity){
            if(pet.adoptionStatus === "available"){
              if(!pet.requesters.some( (request) => { return request.requesterId === identity } )){
                copy.push(pet);  
          }}}
        });
          
        this.petsNearMe = copy.slice(0, 8);

    });

  }

  openPetProfileModal(pet : IpetProfile) {

    const initialState = {
      currentUserId : this.user.rawUser.userId,
      currentUserName : this.user.rawUser.userName,
      petId : pet._id,
      image : pet.petProfilePic,
      name : pet.petName,
      kind : pet.petKind,
      distance : pet.dist.calculated,
      details : pet.petAdditionalDetails
    };

    //onsole.log(initialState);

    this.modalService.show(
      PetProfileModalComponent,
      Object.assign({}, { class: 'gray modal-lg' , initialState})
    );
  }

}