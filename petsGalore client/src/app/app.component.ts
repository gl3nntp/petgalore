import { Component, OnInit} from '@angular/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { SignInComponent } from './sign-in-modal/sign-in.component';
import { UserStatusService } from '../states/user.status.service';
import { IUserAuthenticated } from '../model/user.model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{

  loggedIn : boolean;
  user : string;

  constructor(private modalService: BsModalService,
                private userState : UserStatusService,
                    private route : Router) {}

  ngOnInit(): void {
    this.checkUserAuth();
  }

  checkUserAuth(){
    this.userState.currentUser.subscribe(
      (status : IUserAuthenticated) => {
        //console.log(`app component current user with status: ${status.userName} ${status.isAuthenticated}`);
        this.loggedIn = status.isAuthenticated;
        this.user = status.userName;
        console.log(`app component user authentication status: ${this.loggedIn} user: ${this.user}`)
      }
    );
  }

  logOut(){
    this.userState.userLogOut();
    window.location.reload();
    //this.route
  }

  openSignInModal() {
    const initialState = {
      userComponentValue: '',
      whoCalledSignInModal: 'AppComponent'
    };

    this.modalService.show(SignInComponent,
        Object.assign({}, { class: 'gray modal-md', initialState }));
    }

}
