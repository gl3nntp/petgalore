import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, AbstractControl, FormControl, FormGroup} from '@angular/forms';
import { IUser } from '../../model/user.model';
import { SyncValidatorsService } from '../../formValidatorServices/sync-validators.service';
import { AsyncValidate } from '../../formValidatorServices/asyc-validators.service';
import { UsersService } from '../../httpServices/users.service';
import { SignInComponent } from '../sign-in-modal/sign-in.component';
import { BsModalService } from 'ngx-bootstrap/modal';


@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css'],
  providers: [SyncValidatorsService, UsersService, AsyncValidate]
})
export class RegisterComponent implements OnInit {

  constructor(
    private formBuilder : FormBuilder,
    private syncValidate : SyncValidatorsService,
    private httpUser : UsersService,
    private asyncValidate : AsyncValidate,
    private modalService : BsModalService) { }

  //usersList : IUser[];
  userAdded : IUser;

  newUserForm : FormGroup;

  ngOnInit() {
    this.newUserForm = this.formBuilder.group({
      firstName: ['',Validators.required],
      lastName: ['',Validators.required],
      userName: ['', { 
        validators: [Validators.required],
        asyncValidators: [this.asyncValidate.validateUserName.bind(this.asyncValidate)],
        updateOn: 'blur'
      }],
      email: ['', { 
        validators: [Validators.required, this.syncValidate.ValidateEmailFormat()],
        asyncValidators: [this.asyncValidate.validate.bind(this.asyncValidate)],
        updateOn: 'blur'
      }],
      password: ['', [Validators.required, Validators.minLength(8)]],
      passwordVerify : ['', Validators.required]
      }, {validators: this.syncValidate.ValidatePassword()});
  }

  //form getters
  public get firstName() : AbstractControl { return this.newUserForm.get('firstName'); }
  public get lastName() : AbstractControl { return this.newUserForm.get('lastName'); }
  public get userName() : AbstractControl { return this.newUserForm.get('userName'); }
  public get password() : AbstractControl { return this.newUserForm.get('password'); }
  public get email() : AbstractControl { return this.newUserForm.get('email'); }
  public get passwordVerify() : AbstractControl { return this.newUserForm.get('passwordVerify'); }

  registerUser(){
    /*console.log(`form status: ${this.newUserForm.status}`);
    console.log(this.newUserForm.value);*/

    this.httpUser.addUser(this.newUserForm.value).subscribe((result)=>{

      //register attempts: if true the sign in with the given email
      //user just needs to input password
      if (result === true) {
        this.signIn();
      } else {
        alert(`oops! there was a problem registering, please try again`);
        this.newUserForm.reset();
      };
      
    });
  }

  signIn(){

    //initial state of log-in modal is set to
    //1. caller of the modal, in this case this component
    //2. email of the user
    const initialState = {
      whoCalledSignInModal: 'RegisterComponent',
      userComponentValue: this.email.value
    };

    //modal service is called and modal is shown with the initial state
    //the signInComponent is loaded into the modal service
    this.modalService.show(SignInComponent,
    Object.assign({}, { class: 'gray modal-md', initialState }));

    this.newUserForm.reset();
  }

}
