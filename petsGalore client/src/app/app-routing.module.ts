import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ShopComponent } from './shop/shop.component';
import { AdoptComponent } from './adopt/adopt.component';
import { HomeComponent } from './home/home.component';
import { RegisterComponent } from './register/register.component';
import { UserProfileComponent } from './user-profile/user-profile.component';
import { CreatePetProfileComponent } from './user-profile/create-pet-profile/create-pet-profile.component';

const routes: Routes = [
  { path: 'store', component: ShopComponent },
  { path: 'adopt', component: AdoptComponent },
  { path: 'home', component: HomeComponent},
  { path: '', redirectTo: 'home', pathMatch: 'full'},
  { path: 'register', component: RegisterComponent},
  { path: 'userProfile', component: UserProfileComponent},
  { path: 'createPetProfile', component: CreatePetProfileComponent}];

//UserProfileComponent

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
