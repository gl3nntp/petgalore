import { Component, OnInit, ChangeDetectionStrategy, Input, Output, EventEmitter } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap';
import { BsModalService } from 'ngx-bootstrap/modal';
import { SignInComponent } from '../sign-in-modal/sign-in.component';
import { UserStatusService } from '../../states/user.status.service';
import { UsersService } from '../../httpServices/users.service';
import { IpetProfile, IAdoptionRequest } from '../../model/user.model';
import { Message } from '@angular/compiler/src/i18n/i18n_ast';
import { InterComponentComService } from '../../componentCommunicationServices/inter-component-com.service';

@Component({
  selector: 'app-pet-profile-modal',
  templateUrl: './pet-profile-modal.component.html',
  styleUrls: ['./pet-profile-modal.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PetProfileModalComponent implements OnInit {

  @Output() updateEvent : EventEmitter<any> = new EventEmitter();

  currentUserId : string
  currentUserName : string
  petId : string
  kind : string
  distance : number
  details : string
  image : string;
  name : string;
  modalRef : BsModalRef;

  private islogged : boolean = false;

  constructor(private _modalRef : BsModalRef,
           private modalService : BsModalService,
                   private user : UserStatusService,
                   private http : UsersService,
               private interCom : InterComponentComService) { 
  this.modalRef = this._modalRef;
  }

  ngOnInit() {
    this.user.currentUser.subscribe( (res) => {
      this.islogged = res.isAuthenticated;
    });
  }

  adoptRequest() {

    const requester : IAdoptionRequest = {
      requesterId : this.currentUserId,
      requesterUserName : this.currentUserName,
      requestStatus : 'pending',
      requestRejectMessage : ''
    }

    this.http.requestAdoption( this.petId, requester).subscribe( (result) => {
      const {message, error} = result;
      if(error) alert('ooops there was a problem, please try again');
      else alert(`${message}`);
      this.modalRef.hide();
      //window.location.reload();
      this.updateEvent.emit('update');
      this.interCom.dataPassed('update');
    });

   
  }

  /*openSignInModal() {
    const initialState = {
      userComponentValue: '',
      whoCalledSignInModal: 'PetProfileModalComponent'
    };

    this.modalService.show(SignInComponent,
        Object.assign({}, { class: 'gray modal-md', initialState }));
    }*/

}
