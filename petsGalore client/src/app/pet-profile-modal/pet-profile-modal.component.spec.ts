import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PetProfileModalComponent } from './pet-profile-modal.component';

describe('PetProfileModalComponent', () => {
  let component: PetProfileModalComponent;
  let fixture: ComponentFixture<PetProfileModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PetProfileModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PetProfileModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
