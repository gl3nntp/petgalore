import { Injectable } from '@angular/core';
import { MapsAPILoader } from '@agm/core';
import { ICoordinates } from '../model/location.models';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LocationService {

  constructor(private loadMap : MapsAPILoader) { }

  //getting user location coords with html 5 api
  trackMe(){

    if(navigator.geolocation){
     var loc = new Observable<ICoordinates>( observer => {
      navigator.geolocation.getCurrentPosition( position => {
        const longitude = position.coords.longitude;
        const latitude = position.coords.latitude;
        observer.next({ longitude, latitude });
        observer.complete();
      });
    })
    } else {
      alert("In order for us to suggest pets for you, please turn on Geolocation in your browser");
    };

    return loc;
  }

  //maps api should be loaded firsrt before you can create an instance of the Geocoder
  geoCoder(){
    this.loadMap.load().then(()=>{
      console.log("maps is loaded");
      const convertAddress = new google.maps.Geocoder();
      convertAddress.geocode({
        address: 'ussishkin 68, tel aviv'},
        (results, status) => {
          console.log(`geocoding results latitude: ${results[0].geometry.location.lat()}`);
          console.log(`geocoding status: ${status}`);
        });
    });
  }

}
