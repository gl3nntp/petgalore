export interface IUser {
    id ?: string;
    firstName : string;
    lastName : string;
    userName : string;
    password : string;
    email : string;
}

export interface IUserRegister {
    firstName : string;
    lastName : string;
    userName : string;
    password : string;
    passwordVerify : string;
    email : string;
}

export interface IUserAuthenticated {
    userName : string;
    userId : string;
    webToken : string;
    isAuthenticated ?: boolean;
}

export interface IpetProfile {
    _id ?: string;
    originalOwnerId ?: string;
    newOwnerId ?: string;
    adoptionStatus : 'available' | 'adopted';
    petName : string;
    petKind : string;
    petAdditionalDetails : string;
    petProfilePic : string;
    petAddress : string;
    location : IgeoJSON;
    dist ?: { calculated : number };
    requesters ?: IAdoptionRequest[];
}

export interface IAdoptionRequest {
    _id ?: string;
    requesterId : string;
    requesterUserName : string;
    requestStatus : 'pending' | 'approved' | 'rejected';
    requestRejectMessage : string;
}


//mongo db geoJSON format. coordinates order: index 0 = longitude, index 1 = lattitude
export interface IgeoJSON {
    type ?: 'Point';
    coordinates : [number, number]
}

/*export interface userAdoptionRequests {
    requestId ?: string;
    requesterId : string;
    petId : string;
    statusApproved : boolean;
}*/



