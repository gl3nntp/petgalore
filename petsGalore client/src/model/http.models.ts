export interface IuserAuthServerResult {
    error : boolean;
    result : string;
    userName ?: string;
    userId ?: string;
};